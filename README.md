## REST API for code repositories

The main objective of the project is to create API to search in code stored on GitHub repository and make simple possibility to extend for next sources.

### Getting Started

These instructions will show you how to get a copy of the project up and running on your local machine. See deployment for information, where you can test it online.

### Using

Exposed endpoint:

* /search/{source}/?query=matexx

where **{source}** is one of registered source drivers.

In this project is fully supported /search/github/?query=matexx 
 
I have also registered 

* /search/bitbucket/?query=matexx 

with mocked dummy empty response to provide addition new source example.  

#### Prerequisites

```
* Php >= 7.1.*
* Composer
```

#### Installing

* Required .env file

Just change .env.example => .env filename and fill GITHUB_TOKEN variable with working access token.

```
APP_ENV=local
APP_DEBUG=false
APP_KEY=
APP_TIMEZONE=UTC

LOG_CHANNEL=stack
LOG_SLACK_WEBHOOK_URL=

GITHUB_TOKEN=PASTE_HERE_YOUR_GITHUB_TOKEN
GITHUB_DEFAULT_FIRST_PAGE=1
GITHUB_DEFAULT_PER_PAGE=25
GITHUB_DEFAULT_ORDER=desc
GITHUB_DEFAULT_SORT=updated

CACHE_DRIVER=file
CACHE_TIME=10
QUEUE_DRIVER=sync
```

* composer install - to install all dependencies

* php -S localhost:8080 -t public - to run project locally (Then available on [http://localhost:8080](http://localhost:8080) )

### Adding new source

To add new source you have just to:

* Implement interface: App\SourceDrivers\SourceDriver

```
interface SourceDriver
{
    public function receiveData(Request $request): Result;
} 
```

* Register new source driver in App\Providers\SourceDriverServiceProvider

See BitbucketSourceDriver in below:

```php
$this->app->make(SourceDriverRegistry::class)
    ->register("github", $this->app->make(GithubSourceDriver::class))
    ->register("bitbucket", $this->app->make(BitbucketSourceDriver::class));
```

### Running the tests

Tests location: /tests/

Running tests: ./vendor/bin/phpunit

### Built With

* [Lumen](https://lumen.laravel.com/docs/5.6) - PHP Framework
* [Composer](https://getcomposer.org/doc/) - Dependency Management

### Authors

* **Mateusz Orłowski** - (https://bitbucket.org/matexx/task1)
