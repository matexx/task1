<?php

namespace App\Http\Middleware;

use Closure;
use Laravel\Lumen\Routing\ProvidesConvenienceMethods;

/**
 * Class QueryParametersValidation
 * @package App\Http\Middleware
 */
class QueryParametersValidation
{
    use ProvidesConvenienceMethods;

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function handle($request, Closure $next)
    {
        $this->validate($request, [
            'query' => 'required',
            'page' => 'numeric|min:1',
            'perPage' => 'numeric|min:1',
            'sort' => 'alpha_num',
            'order' => 'nullable|in:asc,desc'
        ]);

        return $next($request);
    }
}
