<?php

namespace App\Http\Controllers;

use App\SourceDrivers\SourceDriverRegistry;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class SearchCodeController
 * @package App\Http\Controllers
 */
class SearchCodeController extends Controller
{
    /**
     * @var SourceDriverRegistry
     */
    public $sourceDriverRegistry;

    /**
     * SearchCodeController constructor.
     * @param SourceDriverRegistry $registry
     */
    function __construct(SourceDriverRegistry $registry)
    {
        $this->sourceDriverRegistry = $registry;
    }

    /**
     * @param Request $request
     * @param string $source
     * @return JsonResponse
     * @throws \Exception
     */
    public function search(Request $request, string $source): JsonResponse
    {
        Log::info('Request: ' . $request->getRequestUri());
        $sourceDriver = $this->sourceDriverRegistry->get($source);
        $result = $sourceDriver->receiveData($request);
        return response()
            ->json($result, 200);
    }
}
