<?php

namespace App\SourceDrivers\Github;

use App\Query\Parameters;
use App\Query\Result;
use App\Query\ResultElement;
use App\SourceDrivers\SourceDriveCache;
use App\SourceDrivers\SourceDriver;
use GrahamCampbell\GitHub\GitHubManager;
use Illuminate\Http\Request;

/**
 * Class GithubSourceDriver
 * @package App\SourceDrivers
 */
class GithubSourceDriver implements SourceDriver
{
    use SourceDriveCache;

    /**
     * @var GitHubManager
     */
    protected $github;

    /**
     * GithubSourceDriver constructor.
     * @param GitHubManager $github
     */
    public function __construct(GitHubManager $github)
    {
        $this->github = $github;
    }

    /**
     * @param Request $request
     * @return Result
     */
    public function receiveData(Request $request): Result
    {
        $params = $this->grabParameters($request);

        $githubResult = $this->getGithubResults($params);

        $sourceDriverResult = new Result();
        $sourceDriverResult->setPage($params->getPage())
            ->setPerPage($params->getPerPage())
            ->setTotalItems(intval($githubResult['total_count']))
            ->setItems($this->convertItems($githubResult['items']));

        return $sourceDriverResult;
    }


    /**
     * @param Request $request
     * @return Parameters
     */
    private function grabParameters(Request $request): Parameters
    {
        $parameters = new Parameters();
        return $parameters->setQuery($request->input('query'))
            ->setPage($request->input('page', env('GITHUB_DEFAULT_FIRST_PAGE')))
            ->setPerPage($request->input('perPage', env('GITHUB_DEFAULT_PER_PAGE')))
            ->setSort($request->input('sort', env('GITHUB_DEFAULT_SORT')))
            ->setOrder($request->input('order', env('GITHUB_DEFAULT_ORDER')));
    }

    /**
     * @param Parameters $params
     * @return array
     */
    private function getGithubResults(Parameters $params): array
    {
        $fromCache = $this->getFromCache($params);
        if ($fromCache !== false) {
            return $fromCache;
        }

        $result = $this->github
            ->search()
            ->setPage($params->getPage())
            ->setPerPage($params->getPerPage())
            ->code(
                $params->getQuery(),
                $params->getSort(),
                $params->getOrder()
            );

        $this->putInCache($params, $result);

        return $result;
    }

    /**
     * @param $items
     * @return array
     */
    private function convertItems($items): array
    {
        $out = [];
        foreach ($items as $item) {
            ;
            $element = new ResultElement();
            $out[] = $element->setOwnerName($item['repository']['owner']['login'])
                ->setRepositoryName($item['repository']['full_name'])
                ->setFileName($item['name']);
        }
        return $out;
    }
}
