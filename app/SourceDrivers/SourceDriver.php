<?php

namespace App\SourceDrivers;

use App\Query\Result;
use Illuminate\Http\Request;

/**
 * Interface SourceDriver
 * @package App\SourceDrivers
 */
interface SourceDriver
{

    /**
     * @param Request $request
     * @return Result
     */
    public function receiveData(Request $request): Result;

}
