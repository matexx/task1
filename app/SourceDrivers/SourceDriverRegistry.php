<?php

namespace App\SourceDrivers;

/**
 * Class SourceDriverRegistry
 * @package App\SourceDrivers
 */
class SourceDriverRegistry
{

    /**
     * @var array
     */
    public $searchers = [];

    /**
     * @param $name
     * @param SourceDriver $instance
     * @return $this
     */
    function register($name, SourceDriver $instance)
    {
        $this->searchers[$name] = $instance;
        return $this;
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    function get($name)
    {
        if (in_array($name, array_keys($this->searchers))) {
            return $this->searchers[$name];
        } else {
            throw new \Exception("SourceDriver " . $name . " missing.");
        }
    }

}
