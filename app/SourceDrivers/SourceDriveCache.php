<?php

namespace App\SourceDrivers;


use App\Query\Parameters;
use Illuminate\Support\Facades\Cache;

/**
 * Trait SourceDriveCache
 * @package App\SourceDrivers
 */
trait SourceDriveCache
{
    /**
     * @param Parameters $params
     * @return bool | mixed
     */
    public function getFromCache(Parameters $params)
    {
        $cacheKey = sha1($params->getQuery() . $params->getPage() . $params->getPerPage() . $params->getSort() . $params->getOrder());
        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }
        return false;
    }

    /**
     * @param Parameters $params
     * @param mixed $data
     */
    public function putInCache(Parameters $params, $data)
    {
        $cacheKey = sha1($params->getQuery() . $params->getPage() . $params->getPerPage() . $params->getSort() . $params->getOrder());
        Cache::put($cacheKey, $data, env('CACHE_TIME', 10));
    }
}