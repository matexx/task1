<?php

namespace App\SourceDrivers\Bitbucket;

use App\Query\Parameters;
use App\Query\Result;
use App\SourceDrivers\SourceDriver;
use Illuminate\Http\Request;

/**
 * Class BitbucketSourceDriver
 * @package App\SourceDrivers\Bitbucket
 */
class BitbucketSourceDriver implements SourceDriver
{

    /**
     * @param Request $request
     * @return Result
     */
    public function receiveData(Request $request): Result
    {
        $params = $this->grabParameters($request);

        $sourceDriverResult = new Result();
        $sourceDriverResult->setPage($params->getPage())
            ->setPerPage($params->getPerPage())
            ->setTotalItems(intval(0))
            ->setItems([]);

        return $sourceDriverResult;
    }

    /**
     * @param Request $request
     * @return Parameters
     */
    private function grabParameters(Request $request): Parameters
    {
        $parameters = new Parameters();
        return $parameters->setQuery($request->input('query'))
            ->setPage($request->input('page', 1))
            ->setPerPage($request->input('per_page', 25))
            ->setSort($request->input('sort', ''))
            ->setOrder($request->input('order', ''));
    }
}