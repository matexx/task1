<?php

namespace App\Query;

/**
 * Class Parameters
 * @package App\Query
 */
class Parameters
{
    /**
     * @var string
     */
    private $query;

    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $perPage;

    /**
     * @var string
     */
    private $order;

    /**
     * @var string
     */
    private $sort;

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query;
    }

    /**
     * @param string $query
     * @return Parameters
     */
    public function setQuery(string $query): Parameters
    {
        $this->query = $query;
        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return Parameters
     */
    public function setPage(int $page): Parameters
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     * @return Parameters
     */
    public function setPerPage(int $perPage): Parameters
    {
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrder(): string
    {
        return $this->order;
    }

    /**
     * @param string $order
     * @return Parameters
     */
    public function setOrder(string $order): Parameters
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return string
     */
    public function getSort(): string
    {
        return $this->sort;
    }

    /**
     * @param string $sort
     * @return Parameters
     */
    public function setSort(string $sort): Parameters
    {
        $this->sort = $sort;
        return $this;
    }

}