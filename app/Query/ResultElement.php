<?php

namespace App\Query;

use JsonSerializable;

/**
 * Class ResultElement
 * @package App\Query
 */
class ResultElement implements JsonSerializable
{
    /**
     * @var string
     */
    private $ownerName;

    /**
     * @var string
     */
    private $repositoryName;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @return string
     */
    public function getOwnerName(): string
    {
        return $this->ownerName;
    }

    /**
     * @param string $ownerName
     * @return ResultElement
     */
    public function setOwnerName(string $ownerName): ResultElement
    {
        $this->ownerName = $ownerName;
        return $this;
    }

    /**
     * @return string
     */
    public function getRepositoryName(): string
    {
        return $this->repositoryName;
    }

    /**
     * @param string $repositoryName
     * @return ResultElement
     */
    public function setRepositoryName(string $repositoryName): ResultElement
    {
        $this->repositoryName = $repositoryName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     * @return ResultElement
     */
    public function setFileName(string $fileName): ResultElement
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}