<?php

namespace App\Providers;

use App\SourceDrivers\Bitbucket\BitbucketSourceDriver;
use App\SourceDrivers\Github\GithubSourceDriver;
use App\SourceDrivers\SourceDriverRegistry;
use Illuminate\Support\ServiceProvider;

class SourceDriverServiceProvider extends ServiceProvider
{

    function register()
    {
        $this->app->singleton(SourceDriverRegistry::class);
    }

    function boot()
    {
        $this->app->make(SourceDriverRegistry::class)
            ->register("github", $this->app->make(GithubSourceDriver::class))
            ->register("bitbucket", $this->app->make(BitbucketSourceDriver::class));
    }

}
