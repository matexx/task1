<?php

use App\Http\Controllers\SearchCodeController;
use App\Query\Result;
use App\SourceDrivers\SourceDriver;
use App\SourceDrivers\SourceDriverRegistry;
use Illuminate\Http\Request;

/**
 * Class SearchCodeControllerTest
 */
class SearchCodeControllerTest extends TestCase
{

    public function testSearchWithParam()
    {
        $request = Request::create('/search?query=queryThatYouCantFind', 'GET');

        $responseMock = new Result();
        $responseMock->setPage(1)
            ->setPerPage(25)
            ->setTotalItems(0)
            ->setItems([]);

        $githubSourceDriverMock = Mockery::mock(SourceDriver::class);
        $githubSourceDriverMock->shouldReceive('receiveData')->with($request)->andReturn($responseMock);
        $this->app->instance(SourceDriver::class, $githubSourceDriverMock);

        $sourceDriverRegistryMock = Mockery::mock(SourceDriverRegistry::class);
        $sourceDriverRegistryMock->shouldReceive('get')->with('exampleSource')->andReturn($githubSourceDriverMock);
        $this->app->instance(SourceDriverRegistry::class, $sourceDriverRegistryMock);

        $controller = $this->app->make(SearchCodeController::class);
        $result = $controller->search($request, 'exampleSource');

        $this->assertEquals(
            json_encode($responseMock), $result->content(),
            'Check response with parameter.'
        );
    }

    public function tearDown()
    {
        Mockery::close();
    }
}
