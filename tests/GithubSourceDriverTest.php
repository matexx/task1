<?php

use App\Query\Result;
use App\Query\ResultElement;
use App\SourceDrivers\Github\GithubSourceDriver;
use GrahamCampbell\GitHub\GitHubManager;
use Illuminate\Http\Request;

/**
 * Class GithubSourceDriverTest
 */
class GithubSourceDriverTest extends TestCase
{

    public function testReceivingData()
    {
        $resultMock = $this->getResultMockWith3Items();

        $expectedItems = $this->getExpected3Items();

        $expectedResult = new Result();
        $expectedResult->setTotalItems(3)
            ->setPage(1)
            ->setPerPage(25)
            ->setItems($expectedItems);

        $request = Request::create('/search?query=exampleQuery', 'GET');

        $gitHubManagerMock = Mockery::mock(GitHubManager::class);
        $gitHubManagerMock->shouldReceive('search')
            ->andReturn($gitHubManagerMock);

        $gitHubManagerMock
            ->shouldReceive('setPage')
            ->with($expectedResult->getPage())
            ->andReturn($gitHubManagerMock);

        $gitHubManagerMock
            ->shouldReceive('setPerPage')
            ->with($expectedResult->getPerPage())
            ->andReturn($gitHubManagerMock);

        $gitHubManagerMock->shouldReceive('code')
            ->with('exampleQuery', '', 'desc')
            ->andReturn($resultMock);

        $this->app->instance(GitHubManager::class, $gitHubManagerMock);

        $githubSourceDriver = $this->app->make(GithubSourceDriver::class);
        $receivedData = $githubSourceDriver->receiveData($request);
        $this->assertEquals($expectedResult->jsonSerialize(), $receivedData->jsonSerialize());
    }

    /**
     * @return array
     */
    private function getResultMockWith3Items()
    {
        return [
            'total_count' => 3,
            'incomplete_results' => false,
            'items' => [
                0 =>
                    [
                        'name' => 'index.html',
                        'path' => 'index.html',
                        'sha' => '72943a16fb2c8f38f9dde202b7a70ccc19c52f34',
                        'url' => 'http://example.url',
                        'git_url' => 'http://example.url',
                        'html_url' => 'http://example.url',
                        'repository' =>
                            [
                                'id' => 654321,
                                'name' => 'example_name',
                                'full_name' => 'example/name',
                                'owner' =>
                                    [
                                        'login' => 'matexx',
                                        'id' => 123456,
                                        'avatar_url' => 'http://example.url',
                                        'gravatar_id' => '',
                                        'url' => 'http://example.url',
                                        'type' => 'User',
                                        'site_admin' => false,
                                    ],
                            ],
                        'score' => 7.2062044,
                    ],
                1 =>
                    [
                        'name' => 'index1.html',
                        'path' => 'index1.html',
                        'sha' => '72943a16fb2c8f38f9dde202b7a70ccc19c52f34',
                        'url' => 'http://example1.url',
                        'git_url' => 'http://example1.url',
                        'html_url' => 'http://example1.url',
                        'repository' =>
                            [
                                'id' => 654321,
                                'name' => 'example_name1',
                                'full_name' => 'example/name1',
                                'owner' =>
                                    [
                                        'login' => 'matexx1',
                                        'id' => 123456,
                                        'avatar_url' => 'http://example1.url',
                                        'gravatar_id' => '',
                                        'url' => 'http://example1.url',
                                        'type' => 'User',
                                        'site_admin' => false,
                                    ],
                            ],
                        'score' => 6.2062044,
                    ],
                2 =>
                    [
                        'name' => 'index2.html',
                        'path' => 'index2.html',
                        'sha' => '72943a16fb2c8f38f9dde202b7a70ccc19c52f34',
                        'url' => 'http://example2.url',
                        'git_url' => 'http://example2.url',
                        'html_url' => 'http://example2.url',
                        'repository' =>
                            [
                                'id' => 654321,
                                'name' => 'example_name2',
                                'full_name' => 'example/name2',
                                'owner' =>
                                    [
                                        'login' => 'matexx2',
                                        'id' => 123456,
                                        'avatar_url' => 'http://example2.url',
                                        'gravatar_id' => '',
                                        'url' => 'http://example2.url',
                                        'type' => 'User',
                                        'site_admin' => false,
                                    ],
                            ],
                        'score' => 5.2062044,
                    ],
            ]
        ];
    }

    /**
     * @return array
     */
    private function getExpected3Items()
    {
        $expectedItem = new ResultElement();
        $expectedItem->setOwnerName('matexx')
            ->setRepositoryName('example/name')
            ->setFileName('index.html');

        $expectedItem1 = new ResultElement();
        $expectedItem1->setOwnerName('matexx1')
            ->setRepositoryName('example/name1')
            ->setFileName('index1.html');

        $expectedItem2 = new ResultElement();
        $expectedItem2->setOwnerName('matexx2')
            ->setRepositoryName('example/name2')
            ->setFileName('index2.html');

        return [$expectedItem, $expectedItem1, $expectedItem2];
    }
}