<?php

use App\Http\Middleware\QueryParametersValidation;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class ObligatoryParametersTest
 */
class QueryParametersValidationTest extends TestCase
{
    public function testResponseWhenQueryParameterIsMissing()
    {
        $request = Request::create('/search', 'GET');
        $this->expectException(ValidationException::class);
        $middleware = new QueryParametersValidation();
        $middleware->handle($request, function () {
        });
    }

    public function testResponseWhenPageParameterIsInWrongFormat()
    {
        $request = Request::create('/search?query=exampleQuery&page=xyz', 'GET');
        $this->expectException(ValidationException::class);
        $middleware = new QueryParametersValidation();
        $middleware->handle($request, function () {
        });
    }

    public function testResponseWhenPerPageParameterIsInWrongFormat()
    {
        $request = Request::create('/search?query=exampleQuery&page=12&perPage=esd', 'GET');
        $this->expectException(ValidationException::class);
        $middleware = new QueryParametersValidation();
        $middleware->handle($request, function () {
        });
    }

    public function testResponseWhenOrderParameterIsInWrongFormat()
    {
        $request = Request::create('/search?query=exampleQuery&page=12&perPage=25&order=xxx', 'GET');
        $this->expectException(ValidationException::class);
        $middleware = new QueryParametersValidation();
        $middleware->handle($request, function () {
        });
    }

    public function testResponseWhenEveryParametersAreOk()
    {
        $request = Request::create('/search?query=aaa&page=10&perPage=3&sort=updated&order=asc', 'GET');
        $middleware = new QueryParametersValidation();
        $called = false;
        $middleware->handle($request, function () use (&$called) {
            $called = true;
        });

        $this->assertTrue($called, 'Should call "next" closure parameter.');
    }
}